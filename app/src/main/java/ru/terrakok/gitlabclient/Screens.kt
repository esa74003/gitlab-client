package ru.terrakok.gitlabclient

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.FragmentFactory
import com.github.terrakok.cicerone.androidx.ActivityScreen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import gitfox.entity.IssueState
import gitfox.entity.MergeRequestState
import gitfox.entity.app.target.TargetAction
import ru.terrakok.gitlabclient.ui.about.AboutFragment
import ru.terrakok.gitlabclient.ui.auth.AuthFlowFragment
import ru.terrakok.gitlabclient.ui.auth.AuthFragment
import ru.terrakok.gitlabclient.ui.commit.CommitFragment
import ru.terrakok.gitlabclient.ui.drawer.DrawerFlowFragment
import ru.terrakok.gitlabclient.ui.file.ProjectFileFragment
import ru.terrakok.gitlabclient.ui.issue.*
import ru.terrakok.gitlabclient.ui.libraries.LibrariesFragment
import ru.terrakok.gitlabclient.ui.main.MainFragment
import ru.terrakok.gitlabclient.ui.mergerequest.*
import ru.terrakok.gitlabclient.ui.my.activity.MyEventsFragment
import ru.terrakok.gitlabclient.ui.my.issues.MyIssuesContainerFragment
import ru.terrakok.gitlabclient.ui.my.issues.MyIssuesFragment
import ru.terrakok.gitlabclient.ui.my.mergerequests.MyMergeRequestsContainerFragment
import ru.terrakok.gitlabclient.ui.my.mergerequests.MyMergeRequestsFragment
import ru.terrakok.gitlabclient.ui.my.todos.MyTodosContainerFragment
import ru.terrakok.gitlabclient.ui.my.todos.MyTodosFragment
import ru.terrakok.gitlabclient.ui.privacypolicy.PrivacyPolicyFragment
import ru.terrakok.gitlabclient.ui.project.MainProjectFragment
import ru.terrakok.gitlabclient.ui.project.ProjectFlowFragment
import ru.terrakok.gitlabclient.ui.project.files.ProjectFilesFragment
import ru.terrakok.gitlabclient.ui.project.info.ProjectEventsFragment
import ru.terrakok.gitlabclient.ui.project.info.ProjectInfoContainerFragment
import ru.terrakok.gitlabclient.ui.project.info.ProjectInfoFragment
import ru.terrakok.gitlabclient.ui.project.issues.ProjectIssuesContainerFragment
import ru.terrakok.gitlabclient.ui.project.issues.ProjectIssuesFragment
import ru.terrakok.gitlabclient.ui.project.labels.ProjectLabelsFragment
import ru.terrakok.gitlabclient.ui.project.members.ProjectMembersFragment
import ru.terrakok.gitlabclient.ui.project.mergerequest.ProjectMergeRequestsContainerFragment
import ru.terrakok.gitlabclient.ui.project.mergerequest.ProjectMergeRequestsFragment
import ru.terrakok.gitlabclient.ui.project.milestones.ProjectMilestonesFragment
import ru.terrakok.gitlabclient.ui.projects.ProjectsContainerFragment
import ru.terrakok.gitlabclient.ui.projects.ProjectsListFragment
import ru.terrakok.gitlabclient.ui.user.UserFlowFragment
import ru.terrakok.gitlabclient.ui.user.info.UserInfoFragment

/**
 * @author Konstantin Tskhovrebov (aka terrakok) on 26.03.17.
 */
object Screens {

    // Flows
    object AuthFlow : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = AuthFlowFragment()
    }

    object DrawerFlow : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = DrawerFlowFragment()
    }

    data class ProjectFlow(
        val projectId: Long
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectFlowFragment.create(projectId)
    }

    data class UserFlow(
        val userId: Long
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = UserFlowFragment.create(userId)
    }

    data class IssueFlow(
        val projectId: Long,
        val issueId: Long,
        val targetAction: TargetAction
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = IssueFlowFragment.create(projectId, issueId, targetAction)
    }

    data class MergeRequestFlow(
        val projectId: Long,
        val mrId: Long,
        val targetAction: TargetAction
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MergeRequestFlowFragment.create(projectId, mrId, targetAction)
    }

    // Screens
    object Main : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MainFragment()
    }

    object MyEvents : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MyEventsFragment()
    }

    object MyIssuesContainer : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MyIssuesContainerFragment()
    }

    data class MyIssues(
        val createdByMe: Boolean,
        val onlyOpened: Boolean
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MyIssuesFragment.create(createdByMe, onlyOpened)
    }

    object MyMrContainer : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MyMergeRequestsContainerFragment()
    }

    data class MyMergeRequests(
        val createdByMe: Boolean,
        val onlyOpened: Boolean
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MyMergeRequestsFragment.create(createdByMe, onlyOpened)
    }

    object MyTodosContainer : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MyTodosContainerFragment()
    }

    data class MyTodos(
        val isPending: Boolean
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MyTodosFragment.create(isPending)
    }

    object ProjectsContainer : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectsContainerFragment()
    }

    data class Projects(
        val mode: Int
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectsListFragment.create(mode)
    }

    object About : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = AboutFragment()
    }

    object Libraries : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = LibrariesFragment()
    }

    object Auth : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = AuthFragment()
    }

    object MainProject : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MainProjectFragment()
    }

    object ProjectInfoContainer : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectInfoContainerFragment()
    }

    object ProjectInfo : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectInfoFragment()
    }

    object ProjectEvents : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectEventsFragment()
    }

    object ProjectIssuesContainer : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectIssuesContainerFragment()
    }

    data class ProjectIssues(
        val issueState: IssueState
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectIssuesFragment.create(issueState)
    }

    object ProjectMergeRequestsContainer : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectMergeRequestsContainerFragment()
    }

    data class ProjectMergeRequests(
        val mrState: MergeRequestState
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectMergeRequestsFragment.create(mrState)
    }

    object ProjectLabels : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectLabelsFragment()
    }

    object ProjectMilestones : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectMilestonesFragment()
    }

    object ProjectMembers : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectMembersFragment()
    }

    object ProjectFiles : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectFilesFragment()
    }

    object UserInfo : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = UserInfoFragment()
    }

    object MainMergeRequest : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MainMergeRequestFragment()
    }

    object MergeRequestDetails : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MergeRequestDetailsFragment()
    }

    object MergeRequestInfo : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MergeRequestInfoFragment()
    }

    object MergeRequestCommits : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MergeRequestCommitsFragment()
    }

    object MergeRequestNotes : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MergeRequestNotesFragment()
    }

    object MergeRequestDiffDataList : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MergeRequestDiffDataListFragment()
    }

    object MainIssue : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = MainIssueFragment()
    }

    object IssueInfo : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = IssueInfoFragment()
    }

    object IssueDetails : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = IssueDetailsFragment()
    }

    object IssueNotes : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = IssueNotesFragment()
    }

    object PrivacyPolicy : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = PrivacyPolicyFragment()
    }

    data class Commit(
        val commitId: String,
        val projectId: Long
    ) : FragmentScreen {

        override fun createFragment(factory: FragmentFactory) = CommitFragment.create(commitId, projectId)
    }

    data class ProjectFile(
        val projectId: Long,
        val filePath: String,
        val fileReference: String
    ) : FragmentScreen {
        override fun createFragment(factory: FragmentFactory) = ProjectFileFragment.create(projectId, filePath, fileReference)
    }

    // External flows
    data class ExternalBrowserFlow(
        val url: String
    ) : ActivityScreen {
        override fun createIntent(context: Context) =
            Intent(Intent.ACTION_VIEW, Uri.parse(url))
    }
}
